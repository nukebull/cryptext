module EncrypText
(encryptext
,decryptext
) where
    
import NukeBull
import Crypto.Cipher.AES (AES256)
import Crypto.Cipher.Types (BlockCipher(..), Cipher(..),IV, makeIV)
import Crypto.Error (throwCryptoError)

import qualified Data.ByteString as B

-- Text length =128


encryptext :: B.ByteString -> B.ByteString -> IO B.ByteString
encryptext msg password = do 

           let vectorIV = B.pack ( take 16 $ B.unpack password )
           let key = B.pack (take 32 ( drop 16 $ B.unpack password ))
           let passwordL31 = B.pack (take 16 ( drop 48 $ B.unpack password ))                     
           let rows = passwordRows $ B.unpack passwordL31
           let cols = passwordCols $ B.unpack passwordL31
           let pass = B.pack( take 16 ( drop 64 $ B.unpack password ))
           let pass2 = B.pack( take 16 ( drop 80 $ B.unpack password ))
           let pass3 = B.pack( take 16 ( drop 96 $ B.unpack password ))
           let pass4 = B.pack( take 16 ( drop 112 $ B.unpack password ))
           text <- entrophyBS 128 (B.unpack msg)
           let mInitIV = makeIV vectorIV
           case mInitIV of
            Nothing -> error "Failed to generate and initialization vector."
            Just initIV -> do
                 let texto = encryptAES256 key initIV text
                 let textoMatrix = orderLv3 16 (B.unpack texto) rows cols pass pass2 pass3 pass4
                 return textoMatrix
   
 -- Text length =128             

decryptext :: B.ByteString -> B.ByteString ->  IO B.ByteString
decryptext msg password = do  
         
           --hola
           let vectorIV = B.pack ( take 16 $ B.unpack password )
           let key = B.pack (take 32 ( drop 16 $ B.unpack password ))
           let passwordL31 = B.pack (take 16 ( drop 48 $ B.unpack password ))                     
           let rows = passwordRows $ B.unpack passwordL31
           let cols = passwordCols $ B.unpack passwordL31
           let pass = B.pack( take 16 ( drop 64 $ B.unpack password ))
           let pass2 = B.pack( take 16 ( drop 80 $ B.unpack password ))
           let pass3 = B.pack( take 16 ( drop 96 $ B.unpack password ))
           let pass4 = B.pack( take 16 ( drop 112 $ B.unpack password ))
           let mInitIV = makeIV vectorIV
           case mInitIV of
            Nothing -> error "Failed to generate and initialization vector."
            Just initIV -> do   
               let textoMatrix = orderLv3Inv 16 (B.unpack msg) rows cols pass pass2 pass3 pass4
               let dtext = decryptAES256 key initIV (textoMatrix)
               let text = decryptEntrophy 128 dtext
               return text
