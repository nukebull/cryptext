import NukeBull 
import EncrypText

import System.Environment
import Data.Char
import Data.Word
import Data.Maybe

import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import qualified Data.Text.IO as TIO

import qualified Data.ByteString as B
import qualified Data.ByteString.Base64 as BS


main = do 
       (comando:_) <- getArgs

       case comando of 

         "encrypt" -> do 
                     (_:fileName10:fileName20:_) <- getArgs
                     let fileName1 = "Keys/" ++ fileName10 ++ ".key"
                     let fileName2 = "CryptMsg/" ++ fileName20 ++ ".cryptext"

                     -- Read Password = 128 bytes = 16 vectorIV + 32 AES256 + 16  Cipher Lv.3.1 + 64 Cipher Lv 3.2
                     source <- B.readFile fileName1

                     -- Text to crypt
                     putStrLn "Text to Encrypt ( Maximum number of characters: 127 ) (ASCII 217 ┘ Forbidden)"                    
                     textoOn <- TIO.getLine 
                     if T.length (textoOn) > 127 
                        then putStrLn $ " Yours Characters are: " ++ (show(T.length (textoOn))) ++" (Number Maximun of characters are 127) "
                        else do
                           texto <- encryptext (E.encodeUtf8 textoOn) source
                           putStrLn "Loading Password -------------- is Ok"
                           putStrLn "Encrypting Text --------------- is Ok" 
                           let base64 = BS.encode texto
                           putStrLn "Encoding Base64 --------------- is Ok"
                           B.writeFile fileName2 base64
                           putStrLn "Writing Encrypted File -------- is OK"
                           putStrLn ""
                           putStrLn ""
                           putStrLn "================================================================="
                           putStrLn "=====   N   N  U  U  K  K  EEEE     BBB   U  U  L    L      ====="
                           putStrLn "=====   NN  N  U  U  K K   E        B  B  U  U  L    L      ====="
                           putStrLn "=====   N N N  U  U  KK    EEE      BBB   U  U  L    L      ====="
                           putStrLn "=====   N  NN  U  U  K K   E        B  B  U  U  L    L      ====="
                           putStrLn "=====   N   N  UUUU  K  K  EEEE     BBB   UUUU  LLLL LLLL   ====="
                           putStrLn "================================================================="
                           putStrLn ""
                           putStrLn ""
                           putStrLn ("    Your text has been encrypted in " ++ (show(fileName2)))
                           putStrLn ""
                           


                    
         "decrypt" -> do 
                     (_:fileName10:fileName20:_) <- getArgs
                     let fileName1 = "Keys/" ++ fileName10 ++ ".key"
                     let fileName2 = "CryptMsg/" ++ fileName20 ++ ".cryptext"

                     -- Read Password = 128 bytes = 16 vectorIV + 32 AES256 + 16  Cipher Lv.3.1 + 64 RandomGen Lv 3.2
                     source <- B.readFile fileName1

                     -- Decrypt   
                     code <- B.readFile fileName2
                     let ebase64 = BS.decode code      
                     
                     case ebase64 of
                      Left (base64) -> putStrLn "Is it a base64-CrypText file? are you sure?"
                      Right (base64)-> do

                       if  B.length base64 == 256   
                        then do 
                            text <- decryptext base64 source
                            -- The 217 ASCII is the end of the text
                            let textoInt =  B.elemIndex 217 text
                            --TIO.putStrLn  (E.decodeUtf8(B.take (fromJust textoInt) text))
                            
                            putStrLn ""
                            putStrLn ""
                            putStrLn "================================================================="
                            putStrLn "=====   N   N  U  U  K  K  EEEE     BBB   U  U  L    L      ====="
                            putStrLn "=====   NN  N  U  U  K K   E        B  B  U  U  L    L      ====="
                            putStrLn "=====   N N N  U  U  KK    EEE      BBB   U  U  L    L      ====="
                            putStrLn "=====   N  NN  U  U  K K   E        B  B  U  U  L    L      ====="
                            putStrLn "=====   N   N  UUUU  K  K  EEEE     BBB   UUUU  LLLL LLLL   ====="
                            putStrLn "================================================================="
                            putStrLn ""
                            putStrLn "                           Decrypted Text:                       "                 
                            putStrLn ""
                            TIO.putStrLn  (E.decodeUtf8(B.take (fromJust textoInt) text))
                            putStrLn ""

                        else do
                            -- No More
                            putStrLn "Is it a Cryptext File?"
                      
         "info" -> do
                     putStrLn "1024 bits encryptation (hardness aes256 chipher) "
                     putStrLn ""                     
                     putStrLn "Encrypt: runhaskell CrypText.hs encrypt FileName(Password) FileName (Crypt Msg)"
                     putStrLn "Decrypt: runhaskell CrypText.hs decrypt FileName(Password) FileName (Crypt Msg)"
                     putStrLn ""
                     putStrLn "Gen. Password: runhaskell KeyGen.hs lazy/crazy/info FileName(Password)"
                     putStrLn ""
                     putStrLn " Passwords is in /Keys directory "
                     putStrLn " Crypted texts is in /CryptMsg directory "
                     putStrLn ""
                

         otherwise ->do
                     putStrLn "Arguments Is Not Valid"
                     putStrLn "Used: runhaskell CrypText.hs encrypt FileName(Password) FileName (Crypt Msg)"
                     putStrLn "Used: runhaskell CrypText.hs decrypt FileName(Password) FileName (Crypt Msg)"
