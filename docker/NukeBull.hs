module NukeBull
(randomB
,entrophyBS
,encryptAES256
,decryptAES256
,decryptEntrophy
,noOrder
,passwordRows
,passwordCols,
orderLv3,
orderLv3Inv
) where

import System.Random
import Data.List
import Data.Word
import Data.Bits
import Data.Char
import qualified Data.ByteString as B
import Crypto.Cipher.AES (AES256)
import Crypto.Cipher.Types (BlockCipher(..), Cipher(..),IV, makeIV)
import Crypto.Error (throwCryptoError)
import Crypto.Random(getSystemDRG, randomBytesGenerate)
import Crypto.KDF.Scrypt (generate, Parameters(..))

import qualified Crypto.Random.Entropy as E

-- Random Numbers for Cryptography

randomB :: Int -> IO B.ByteString
randomB n = do
    code <- E.getEntropy n
    return code

---- Expand text

-- Extend the text for 127 bits
-- Char '217' is the character control

extendBS :: Int -> [Word8] -> IO B.ByteString
extendBS n a = do 
    rando <- randomB n
    if n > B.length (B.pack a) then
     return (B.pack (a ++ [217] ++ (B.unpack  (B.take (n -1 - B.length (B.pack a)) (rando)))))
    else return (B.pack a)
       

-- Random n xor [(extendBS n a) xor (RandomB n)]

entrophyBS :: Int -> [Word8] -> IO B.ByteString
entrophyBS n a = do 
    a1 <- extendBS n a 
    a2 <- randomB n
    if n > B.length (B.pack a) then 
     return (B.pack ((B.unpack a2) ++ (zipWith xor (B.unpack a1) (B.unpack a2))))
    else return (B.pack a)


-- entrophyBS decrypt function

decryptEntrophy :: Int -> B.ByteString -> B.ByteString 
decryptEntrophy n a = 
    let a1 = B.unpack $ B.take n a 
        a2 = B.unpack $ B.drop n a       
    in B.pack (zipWith xor a1 a2)


-- AES ciPher Lv2

encryptAES256 ::  B.ByteString -> IV AES256 -> B.ByteString  -> B.ByteString
encryptAES256 key cIV plainData = cbcEncrypt ctx cIV plainData
    where ctx :: AES256
          ctx = throwCryptoError $ cipherInit key          

decryptAES256 :: B.ByteString -> IV AES256 -> B.ByteString  -> B.ByteString
decryptAES256 key cIV plainData = cbcDecrypt ctx cIV plainData
    where ctx :: AES256
          ctx = throwCryptoError $ cipherInit key 


-- Coling Parcival's key derivation function

-- Parameters

paramN = 32:: Word64
paramR = 8
paramP = 1
paramKeyLen = 256

--Scrypt KDF - Coling Parcival's key derivation function
deriveKey :: B.ByteString -> B.ByteString -> B.ByteString
deriveKey password salt = generate params  password salt
    where params = Parameters {n = paramN, r = paramR, p = paramP, outputLength = paramKeyLen}
  

-- Transform  bytes 8bits in two number (0-15) (4 bits numbers) 
-- Similary hexadecimal transform

passwordRows :: [Word8] -> [Int]
passwordRows x = map (`div` 16) (map fromIntegral x)

passwordCols :: [Word8] -> [Int]
passwordCols x = map (`mod` 16) (map fromIntegral x)


-- Matrix translations n = Int deplazament , a = (list Word8) 'for 16 element'

moveBytes :: Int -> [Word8] -> [Word8]
moveBytes n [] = []
moveBytes n a = drop n a ++ take n a

-- moveBytes inverse 'for 16 element'

moveInvBytes :: Int-> [Word8] -> [Word8]
moveInvBytes n [] = []
moveInvBytes n a = drop (16-n) a ++ take (16-n) a

-- moveBytes  and moveInvBytes Matrix 16*16 version

rotFileBytes :: [Int]-> [[Word8]] -> [[Word8]]
rotFileBytes x y = [ moveBytes (x !! n) (y !! n) | n <- [0..15]]

rotFileInvBytes :: [Int]-> [[Word8]] -> [[Word8]]
rotFileInvBytes x y = [ moveInvBytes (x !! n) (y !! n) | n <- [0..15]]

--  Generate a  Matrix 16x16 [[Word8]] with 256 bytes [Word8]

matrixBytes :: [Word8] -> [[Word8]]
matrixBytes [] = []
matrixBytes x = [ take 16 (drop (n*16) x)| n <- [0..15]]

--  Generate a 256 bytes Word8 with Matrix 16x16 

matrix :: [[Word8]] -> [Word8]
matrix [] = []
matrix (x:xs) = x ++ matrix xs

-- Transpose matrix

matrixTransBytes = transpose.matrixBytes

-- Moving elements in Matrix 16x16 s1 move files and s2 move colums

noOrder :: [Word8] ->[Int] -> [Int] -> [Word8]
noOrder c s1 s2   = matrix (matrixTransBytes (matrix (rotFileBytes s2  (matrixTransBytes (matrix (rotFileBytes s1  (matrixBytes c)))))))

-- noOrder decrypt

noOrderInv :: [Word8] ->[Int] -> [Int] -> [Word8]
noOrderInv c s1 s2   = matrix (rotFileInvBytes s1  (matrixBytes ( matrix (matrixTransBytes (matrix (rotFileInvBytes s2  (matrixTransBytes c)))))))

-- 16 interactions, interaction number n

noOrderLv3 ::Int -> [Word8] ->[Int] -> [Int] -> B.ByteString -> B.ByteString -> B.ByteString -> B.ByteString -> B.ByteString
noOrderLv3 n c s1 s2 pass pass2 pass3 pass4 = B.pack ( B.zipWith (xor) (passL3 n pass pass2 pass3 pass4) (B.pack (noOrder c s1 s2)))

-- noOrderLv3 decrypt
-- 16 interactions, interaction number n

noOrderLv3Inv :: Int -> [Word8] ->[Int] -> [Int] -> B.ByteString -> B.ByteString-> B.ByteString -> B.ByteString -> B.ByteString
noOrderLv3Inv  n c s1 s2 pass pass2 pass3 pass4 = B.pack (noOrderInv (B.zipWith (xor) (passL3 (17 - n) pass pass2 pass3 pass4) (B.pack c) ) s1 s2)
      
-- All Interactions (16 = n)

orderLv3 :: Int -> [Word8] ->[Int] -> [Int] -> B.ByteString -> B.ByteString -> B.ByteString -> B.ByteString -> B.ByteString
orderLv3 1 c s1 s2 pass pass2 pass3 pass4 = noOrderLv3 1 c s1 s2 pass pass2 pass3 pass4
orderLv3 n c s1 s2 pass pass2 pass3 pass4 = noOrderLv3 n (B.unpack(orderLv3 (n-1) c s1 s2 pass pass2 pass3 pass4)) s1 s2 pass pass2 pass3 pass4

-- orderLv3 decrypt

orderLv3Inv :: Int -> [Word8] ->[Int] -> [Int] -> B.ByteString-> B.ByteString -> B.ByteString -> B.ByteString -> B.ByteString
orderLv3Inv 1 c s1 s2 pass pass2 pass3 pass4 = noOrderLv3Inv  1 c s1 s2 pass pass2 pass3 pass4
orderLv3Inv n c s1 s2 pass pass2 pass3 pass4 = noOrderLv3Inv  n (B.unpack(orderLv3Inv (n-1) c s1 s2 pass pass2 pass3 pass4)) s1 s2 pass pass2 pass3 pass4



-- Pass 256 Lv 1 for adiction

-- This funtions gen matrix 256 (16*16) bytes for n = 16, With two 16 bytes ByteString keys

passMatrixRow :: Int -> B.ByteString -> B.ByteString
passMatrixRow n a  = B.concatMap  (B.replicate n) a

passMatrixCols :: Int -> B.ByteString -> B.ByteString
passMatrixCols 1 a = a
passMatrixCols n a = B.pack ( (B.unpack (passMatrixRow (n-1) a)) ++ (B.unpack a))

passMatrix :: Int -> B.ByteString -> B.ByteString -> B.ByteString
passMatrix n a b = B.pack ( B.zipWith (xor) (passMatrixRow n a) (passMatrixCols n b) ) 

-- Gen n keys of 256 bytes using Scrypt KDF - Coling Parcival's key derivation function
-- In this aplication used 4*16 bytes keys and passMatrix
-- first and second kays gen password 256bytes for Scrypt KDF
-- third y fourth keys gen 16 salt for Scrypt KDF

passL3 :: Int -> B.ByteString -> B.ByteString -> B.ByteString -> B.ByteString -> B.ByteString
passL3 n a b c d = deriveKey (passMatrix 16 a b) (B.pack (take 16 (drop (16*(n-1)) (B.unpack (passMatrix 16 c d) ) ) ) )