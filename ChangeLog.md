# Changelog for CrypText


## Alpha 0.2
Corrected bug special character (Data.Text.Encoding)
Creating the application KeyGen with more functions
File crypt -> soon 


## Alpha 0.1.3.

Creates 128/256 bytes files automatically (Depending on the length of the text).  
Some Bugs Fixed  

## Alpha 0.1.2.

Added Base64 Encode-Decode  
Added point check in the end of writen text  
added Keys and CryptMsg Directories  
## Alpha 0.1.0.

CryptText Entrophy ++ AES256 (CBC)  
